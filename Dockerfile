FROM golang:1.23-bookworm AS builder
ARG CI_COMMIT_SHA=unknown
WORKDIR /go/src/gitlab.com/cyverse/terraform-http-backend
COPY . .
RUN go build -o terraform-http-backend -ldflags="-X main.GitCommit=${CI_COMMIT_SHA}" .

FROM gcr.io/distroless/base-debian12:nonroot
WORKDIR /
COPY --from=builder /go/src/gitlab.com/cyverse/terraform-http-backend/terraform-http-backend /
CMD ["/terraform-http-backend"]

