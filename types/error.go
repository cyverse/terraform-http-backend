package types

import (
	"errors"
)

// ErrTFStateNotFound ...
var ErrTFStateNotFound = errors.New("state not found")

// ErrTFStateAlreadyLocked ...
var ErrTFStateAlreadyLocked = errors.New("already locked")

// ErrTFStateLockedByOthers ...
var ErrTFStateLockedByOthers = errors.New("locked by others")
