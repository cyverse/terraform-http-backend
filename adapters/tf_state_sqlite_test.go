package adapters

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/terraform-http-backend/ports"
)

func TestTFStateStorageSqliteImplementPort(t *testing.T) {
	var storage ports.StateStorage = &TFStateStorageSqlite{}
	assert.NotNil(t, storage)
}

func TestTFStateStorageSqlite(t *testing.T) {
	TestTFStateStorage(t, func() TFStateStorageCommon {
		return NewTFStateStorageSqliteMem()
	})
}

func TestTFStateStorageSqlite_serializeTime(t *testing.T) {
	var storage TFStateStorageSqlite
	t1 := time.Now()
	tStr, err := storage.serializeTime(t1)
	if !assert.NoError(t, err) {
		return
	}
	t2, err := storage.deserializeTime(tStr)
	if !assert.NoError(t, err) {
		return
	}
	assert.Equal(t, t1.UTC(), t2.UTC())
}
