package adapters

import (
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"time"

	// sqlite driver
	_ "github.com/mattn/go-sqlite3"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/terraform-http-backend/types"
)

// TFStateSchema ...
// This is not used for anything, just simply document the schema of the table.
type TFStateSchema struct {
	ID        int       `psql:"id"`
	StateName string    `psql:"state_name"`
	Owner     string    `psql:"owner"`
	State     *string   `psql:"state"`
	Lock      bool      `psql:"lock"`
	SessionID string    `psql:"session_id"`
	CreatedAt time.Time `psql:"created_at"`
	UpdatedAt time.Time `psql:"updated_at"`
}

// TFHistorySchema ...
// This is not used for anything, just simply document the schema of the table.
type TFHistorySchema struct {
	StateName string    `psql:"state_name"`
	Owner     string    `psql:"owner"`
	State     *string   `psql:"state"`
	CreatedAt time.Time `psql:"created_at"`
}

// TFStateStorageSqlite is a sqlite Terraform State Storage
type TFStateStorageSqlite struct {
	dbFilename string
	db         *sql.DB
}

// NewTFStateStorageSqlite ...
func NewTFStateStorageSqlite(filename string) *TFStateStorageSqlite {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "NewTFStateStorageSqlite",
	})
	db, err := sql.Open("sqlite3", filename)
	if err != nil {
		logger.Fatal(err)
	}

	store := TFStateStorageSqlite{
		dbFilename: filename,
		db:         db,
	}
	err = store.InitTable()
	if err != nil {
		logger.Fatal(err)
	}

	return &store
}

// NewTFStateStorageSqliteMem ...
func NewTFStateStorageSqliteMem() *TFStateStorageSqlite {
	return NewTFStateStorageSqlite(":memory:")
}

// InitTable ...
func (storage *TFStateStorageSqlite) InitTable() error {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "TFStateStorageSqlite.InitTable",
	})
	exists, err := storage.checkIfTableExists("tf_state")
	if err != nil {
		return err
	}
	if !exists {
		err = storage.createStateTable()
		if err != nil {
			return err
		}
		logger.Infof("created table %s", "tf_state")
	}

	exists, err = storage.checkIfTableExists("tf_history")
	if err != nil {
		return err
	}
	if !exists {
		err = storage.createHistoryTable()
		if err != nil {
			return err
		}
		logger.Infof("created table %s", "tf_history")
	}
	return nil
}

func (storage *TFStateStorageSqlite) checkIfTableExists(tableName string) (bool, error) {
	return sqliteCheckIfTableExists(storage.db, tableName)
}

func (storage *TFStateStorageSqlite) createStateTable() error {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "TFStateStorageSqlite.createTable",
	})
	sqlStmt := `CREATE TABLE IF NOT EXISTS tf_state (
		id INTEGER NOT NULL PRIMARY KEY,
		owner TEXT NOT NULL,
		state_name TEXT NOT NULL,
		state TEXT,
		lock BOOLEAN NOT NULL CHECK (lock IN(0, 1)),
		session_id TEXT NOT NULL,
		created_at TEXT NOT NULL,
		updated_at TEXT
		);`
	_, err := storage.db.Exec(sqlStmt)
	if err != nil {
		logger.Errorf("%q: %s\n", err, sqlStmt)
		return err
	}

	sqlStmt = `CREATE UNIQUE INDEX IF NOT EXISTS index_tf_state_owner_name ON tf_state (owner, state_name);`
	_, err = storage.db.Exec(sqlStmt)
	if err != nil {
		logger.Errorf("%q: %s\n", err, sqlStmt)
		return err
	}
	return nil
}

func (storage *TFStateStorageSqlite) createHistoryTable() error {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "TFStateStorageSqlite.createHistoryTable",
	})
	sqlStmt := `CREATE TABLE IF NOT EXISTS tf_history (
		owner TEXT NOT NULL,
		state_name TEXT NOT NULL,
		state TEXT,
		created_at TEXT NOT NULL
		);`
	_, err := storage.db.Exec(sqlStmt)
	if err != nil {
		logger.Errorf("%q: %s\n", err, sqlStmt)
		return err
	}

	sqlStmt = `CREATE INDEX IF NOT EXISTS index_tf_history_owner_name ON tf_history (owner, state_name);`
	_, err = storage.db.Exec(sqlStmt)
	if err != nil {
		logger.Errorf("%q: %s\n", err, sqlStmt)
		return err
	}
	return nil
}

// Lookup ...
func (storage TFStateStorageSqlite) Lookup(owner, stateName string) bool {
	logger := log.WithFields(log.Fields{
		"package":   "adapters",
		"function":  "TFStateStorageSqlite.Lookup",
		"owner":     owner,
		"stateName": stateName,
	})
	row := storage.db.QueryRow("SELECT id FROM tf_state WHERE owner = ? AND state_name = ?", owner, stateName)
	var id uint
	err := row.Scan(&id)
	if errors.Is(err, sql.ErrNoRows) {
		return false
	} else if err != nil {
		logger.WithError(err).Error()
		return false
	}
	return true
}

// Fetch ...
func (storage TFStateStorageSqlite) Fetch(owner, stateName string) (*types.TerraformState, error) {
	logger := log.WithFields(log.Fields{
		"package":   "adapters",
		"function":  "TFStateStorageSqlite.Fetch",
		"owner":     owner,
		"stateName": stateName,
	})
	row := storage.db.QueryRow("SELECT state FROM tf_state WHERE owner = ? AND state_name = ?", owner, stateName)
	var tfStateStr *string
	err := row.Scan(&tfStateStr)
	if errors.Is(err, sql.ErrNoRows) {
		return nil, types.ErrTFStateNotFound
	} else if err != nil {
		logger.WithError(err).Error()
		return nil, err
	}
	if tfStateStr == nil || len(*tfStateStr) == 0 {
		return nil, nil
	}
	var tfState types.TerraformState
	err = json.Unmarshal([]byte(*tfStateStr), &tfState)
	if err != nil {
		logger.WithError(err).Errorf("fail to unmarshal state as JSON")
		return nil, err
	}
	return &tfState, nil
}

// List ...
func (storage TFStateStorageSqlite) List(owner string) ([]string, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "TFStateStorageSql.List",
		"owner":    owner,
	})
	rows, err := storage.db.Query("SELECT state_name FROM tf_state WHERE owner = ?", owner)
	if err != nil {
		logger.WithError(err).Error()
		return nil, err
	}
	defer rows.Close()
	stateNames := make([]string, 0)
	for rows.Next() {
		var name string
		err = rows.Scan(&name)
		if err != nil {
			return nil, err
		}
		stateNames = append(stateNames, name)
	}
	return stateNames, nil
}

// ListHistories list histories of TF state for a state name.
func (storage TFStateStorageSqlite) ListHistories(owner, stateName string) ([]types.TerraformStateHistory, error) {
	logger := log.WithFields(log.Fields{
		"package":   "adapters",
		"function":  "TFStateStorageSqlite.ListHistories",
		"owner":     owner,
		"stateName": stateName,
	})
	sqlStmt := `SELECT state, created_at FROM tf_history WHERE owner = ? AND state_name = ?`
	rows, err := storage.db.Query(sqlStmt, owner, stateName)
	if err != nil {
		logger.WithError(err).Error()
		return nil, err
	}
	if rows == nil {
		return nil, errors.New("rows is nil")
	}
	histories := make([]types.TerraformStateHistory, 0)
	for rows.Next() {
		var history types.TerraformStateHistory
		var stateStr string
		var timeStr string
		err = rows.Scan(&stateStr, &timeStr)
		if err != nil {
			logger.WithError(err).Error("scan failed")
			return nil, err
		}
		err = history.CreatedAt.UnmarshalText([]byte(timeStr))
		if err != nil {
			logger.WithError(err).Error("fail to unmarshal time string")
			return nil, err
		}
		err = json.Unmarshal([]byte(stateStr), &history.State)
		if err != nil {
			logger.WithError(err).Error("fail to unmarshal state string")
			return nil, err
		}
		histories = append(histories, history)
	}
	return histories, nil
}

// Create ...
func (storage TFStateStorageSqlite) Create(sessionID, owner, stateName string, state types.TerraformState, opts ...types.StateCreateOption) (*types.TerraformState, error) {
	logger := log.WithFields(log.Fields{
		"package":   "adapters",
		"function":  "TFStateStorageSqlite.Create",
		"owner":     owner,
		"stateName": stateName,
	})
	marshal, err := json.Marshal(state)
	if err != nil {
		logger.WithError(err).Error()
		return nil, err
	}
	timeNow := storage.timeNow()
	tx, err := storage.db.Begin()
	if err != nil {
		logger.WithError(err).Error("fail to begin transaction")
		return nil, err
	}
	err = storage.transactionInsertTFState(tx, owner, stateName, string(marshal), timeNow)
	if err != nil {
		logger.WithError(err).Error()
		tx.Rollback()
		return nil, err
	}
	// clear histories
	_, err = tx.Exec(`DELETE FROM tf_history WHERE owner = ? and state_name = ?;`, owner, stateName)
	if err != nil {
		logger.WithError(err).Error()
		tx.Rollback()
		return nil, err
	}
	err = tx.Commit()
	if err != nil {
		logger.WithError(err).Error("fail to commit transaction")
		return nil, err
	}
	fetchedState, err := storage.Fetch(owner, stateName)
	if err != nil {
		logger.WithError(err).Error("fail to fetch after create")
		return nil, err
	}
	return fetchedState, nil
}

// UpdateOrCreate ...
func (storage TFStateStorageSqlite) UpdateOrCreate(sessionID, owner, stateName string, state types.TerraformState) (created bool, err error) {
	logger := log.WithFields(log.Fields{
		"package":   "adapters",
		"function":  "TFStateStorageSqlite.UpdateOrCreate",
		"owner":     owner,
		"stateName": stateName,
	})
	marshal, err := json.Marshal(state)
	if err != nil {
		logger.WithError(err).Error()
		return false, err
	}
	tx, err := storage.db.Begin()
	if err != nil {
		logger.WithError(err).Error("fail to begin transaction")
		return false, err
	}
	created, err = storage.updateOrCreate(logger, tx, sessionID, owner, stateName, string(marshal))
	if err != nil {
		return false, err
	}
	err = tx.Commit()
	if err != nil {
		logger.WithError(err).Error("fail to commit transaction")
		return false, err
	}
	fetched, err := storage.Fetch(owner, stateName)
	if err != nil {
		logger.WithError(err).Error()
		return created, err
	}
	if fetched == nil {
		logger.WithField("state", fetched).Debug()
	} else {
		logger.WithField("state", *fetched).Debug()
	}
	return created, nil
}

func (storage TFStateStorageSqlite) updateOrCreate(logger *log.Entry, tx *sql.Tx, sessionID, owner, stateName, state string) (created bool, err error) {
	timeNow := storage.timeNow()
	exists, modifiable, err := storage.transactionCheckStateModifiable(tx, sessionID, owner, stateName)
	if err != nil {
		logger.WithError(err).Error("fail to check if state is modifiable, rolling back")
		tx.Rollback()
		return false, err
	}
	if !exists {
		err = storage.transactionInsertTFState(tx, owner, stateName, state, timeNow)
		if err != nil {
			logger.WithError(err).Error("fail to insert state, rolling back")
			tx.Rollback()
			return false, err
		}
		return true, nil
	} else if modifiable {
		// only insert history if the state is updated (not created)
		err = storage.transactionInsertHistory(tx, owner, stateName, timeNow)
		if err != nil {
			logger.WithError(err).Error("fail to insert history, rolling back")
			tx.Rollback()
			return false, err
		}
		// only update if not locked, or locked by the same session
		err = storage.transactionUpdateTFState(tx, owner, stateName, state, timeNow)
		if err != nil {
			logger.WithError(err).Error("fail to update state, rolling back")
			tx.Rollback()
			return false, err
		}
	} else {
		// not modifiable
		tx.Rollback()
		return false, types.ErrTFStateLockedByOthers
	}
	return false, nil
}

// Check if a state exists and if it is modifiable.
// A state is only modifiable if it is not locked, or it is locked by the same session ID that now attempts to modify it.
// Modification means operation like update/delete/lock/unlock.
func (storage TFStateStorageSqlite) transactionCheckStateModifiable(tx *sql.Tx, sessionID, owner, stateName string) (exist, modifiable bool, err error) {
	sqlStmt := `SELECT lock, session_id FROM tf_state WHERE owner = ? AND state_name = ?;`
	row := tx.QueryRow(sqlStmt, owner, stateName)
	var locked bool
	var fetchedSessionID string
	err = row.Scan(&locked, &fetchedSessionID)
	if errors.Is(err, sql.ErrNoRows) {
		return false, false, nil
	} else if err != nil {
		return false, false, err
	}
	if !locked {
		return true, true, nil
	} else if fetchedSessionID != "" && fetchedSessionID == sessionID {
		return true, true, nil
	} else {
		return true, false, nil
	}

}

func (storage TFStateStorageSqlite) transactionInsertTFState(tx *sql.Tx, owner, stateName, state string, timeNow time.Time) error {
	timeMarshaled, err := storage.serializeTime(timeNow)
	if err != nil {
		return err
	}
	sqlStmt := `INSERT INTO tf_state (owner, state_name, state, lock, session_id, created_at, updated_at) VALUES (?, ?, ?, 0, '', ?, NULL);`
	_, err = tx.Exec(sqlStmt, owner, stateName, state, timeMarshaled)
	if err != nil {
		return err
	}
	return nil
}

func (storage TFStateStorageSqlite) transactionUpdateTFState(tx *sql.Tx, owner, stateName, state string, timeNow time.Time) error {
	timeMarshaled, err := storage.serializeTime(timeNow)
	if err != nil {
		return err
	}
	sqlStmt := `UPDATE tf_state SET state = ?, updated_at = ? WHERE owner = ? AND state_name = ?;`
	_, err = tx.Exec(sqlStmt, state, timeMarshaled, owner, stateName)
	if err != nil {
		return err
	}
	return nil
}

func (storage TFStateStorageSqlite) transactionInsertHistory(tx *sql.Tx, owner, stateName string, timeNow time.Time) error {
	timeMarshaled, err := storage.serializeTime(timeNow)
	if err != nil {
		return err
	}
	sqlStmt := `INSERT INTO tf_history (owner, state_name, state, created_at)
SELECT owner, state_name, state, ? FROM tf_state WHERE owner = ? AND state_name = ?;`
	_, err = tx.Exec(sqlStmt, timeMarshaled, owner, stateName)
	if err != nil {
		return err
	}
	return nil
}

// func (storage TFStateStorageSqlite) transactionInsertHistoryWithState(tx *sql.Tx, owner, stateName, state string, timeNow time.Time) error {
// 	timeMarshaled, err := storage.serializeTime(timeNow)
// 	if err != nil {
// 		return err
// 	}
// 	sqlStmt := `INSERT INTO tf_history (owner, state_name, state, created_at) VALUES (?, ?, ?, ?);`
// 	_, err = tx.Exec(sqlStmt, owner, stateName, state, timeMarshaled)
// 	if err != nil {
// 		return err
// 	}
// 	return nil
// }

// UpdateExisting ...
func (storage TFStateStorageSqlite) UpdateExisting(sessionID, owner, stateName string, state types.TerraformState) (*types.TerraformState, error) {
	logger := log.WithFields(log.Fields{
		"package":   "adapters",
		"function":  "TFStateStorageSqlite.UpdateExisting",
		"owner":     owner,
		"stateName": stateName,
	})

	marshal, err := json.Marshal(state)
	if err != nil {
		logger.WithError(err).Error()
		return nil, err
	}
	tx, err := storage.db.Begin()
	if err != nil {
		logger.WithError(err).Error()
		return nil, err
	}
	err = storage.updateExisting(logger, tx, sessionID, owner, stateName, string(marshal))
	if err != nil {
		tx.Rollback()
		return nil, err
	}
	err = tx.Commit()
	if err != nil {
		logger.WithError(err).Error()
		return nil, err
	}
	return nil, nil
}

func (storage TFStateStorageSqlite) updateExisting(logger *log.Entry, tx *sql.Tx, sessionID, owner, stateName string, state string) error {
	timeNow := storage.timeNow()
	exists, modifiable, err := storage.transactionCheckStateModifiable(tx, sessionID, owner, stateName)
	if err != nil {
		logger.WithError(err).Error()
		return err
	}
	if !exists {
		err = types.ErrTFStateNotFound
		logger.WithError(err).Error("state not found")
		return err
	}
	if !modifiable {
		err = types.ErrTFStateLockedByOthers
		logger.WithError(err).Error("not modifiable")
		return err
	}
	err = storage.transactionInsertHistory(tx, owner, stateName, timeNow)
	if err != nil {
		logger.WithError(err).Error("fail to insert history")
		return err
	}
	err = storage.transactionUpdateTFState(tx, owner, stateName, state, timeNow)
	if err != nil {
		logger.WithError(err).Error("fail to update state")
		return err
	}
	return nil
}

// Delete ...
func (storage TFStateStorageSqlite) Delete(sessionID, owner, stateName string) error {
	logger := log.WithFields(log.Fields{
		"package":   "adapters",
		"function":  "TFStateStorageSqlite.Delete",
		"owner":     owner,
		"stateName": stateName,
	})
	tx, err := storage.db.Begin()
	if err != nil {
		logger.WithError(err).Error("fail to start transaction")
		return err
	}
	exists, modifiable, err := storage.transactionCheckStateModifiable(tx, sessionID, owner, stateName)
	if err != nil {
		logger.WithError(err).Error("fail to check if state is modifiable")
		tx.Rollback()
		return err
	}
	if !exists {
		tx.Rollback()
		return types.ErrTFStateNotFound
	}
	if !modifiable {
		// not modifiable
		err = types.ErrTFStateLockedByOthers
		logger.WithError(err).Error("not modifiable")
		tx.Rollback()
		return err
	}
	err = storage.transactionInsertHistory(tx, owner, stateName, storage.timeNow())
	if err != nil {
		logger.WithError(err).Error("fail to insert history")
		tx.Rollback()
		return err
	}
	err = storage.transactionDeleteState(tx, owner, stateName)
	if err != nil {
		logger.WithError(err).Error("fail to delete state")
		tx.Rollback()
		return err
	}
	err = tx.Commit()
	if err != nil {
		logger.WithError(err).Error("fail to commit transaction")
		return err
	}
	return nil
}

// func (storage TFStateStorageSqlite) transactionFetchState(tx *sql.Tx, owner, stateName string) (exist bool, state string, err error) {
// 	sqlStmt := `SELECT state FROM tf_state WHERE owner = ? AND state_name = ?;`
// 	row := tx.QueryRow(sqlStmt, owner, stateName)
// 	err = row.Scan(&state)
// 	if errors.Is(err, sql.ErrNoRows) {
// 		return false, "", nil
// 	} else if err != nil {
// 		return false, "", err
// 	}
// 	return true, state, nil
// }

func (storage TFStateStorageSqlite) transactionDeleteState(tx *sql.Tx, owner, stateName string) error {
	_, err := tx.Exec("DELETE FROM tf_state WHERE owner = ? AND state_name = ?", owner, stateName)
	if err != nil {
		return err
	}
	return nil
}

// DeleteHistories delete all histories of a state name.
func (storage TFStateStorageSqlite) DeleteHistories(owner, stateName string) (bool, error) {
	logger := log.WithFields(log.Fields{
		"package":   "adapters",
		"function":  "TFStateStorageSqlite.DeleteHistories",
		"owner":     owner,
		"stateName": stateName,
	})
	sqlStmt := `DELETE FROM tf_history WHERE owner = ? AND state_name = ?`
	results, err := storage.db.Exec(sqlStmt, owner, stateName)
	if err != nil {
		logger.WithError(err).Error("query failed")
		return false, err
	}
	affected, err := results.RowsAffected()
	if err != nil {
		logger.WithError(err).Error("fail to obtain rows affected")
		return false, err
	}
	if affected > 0 {
		return true, nil
	}
	return false, nil
}

// Lock ...
func (storage TFStateStorageSqlite) Lock(sessionID, owner, stateName string) error {
	logger := log.WithFields(log.Fields{
		"package":   "adapters",
		"function":  "TFStateStorageSqlite.Lock",
		"owner":     owner,
		"stateName": stateName,
	})

	tx, err := storage.db.Begin()
	if err != nil {
		logger.WithError(err).Error("fail to begin transaction")
		return err
	}
	exists, locked, _, err := storage.transactionLookupState(tx, owner, stateName)
	if err != nil {
		logger.WithError(err).Error("unable to look up state")
		tx.Rollback()
		return err
	}
	if !exists {
		// state not exists, insert placeholder
		err = storage.transactionInsertLockedPlaceHolder(tx, sessionID, owner, stateName)
		if err != nil {
			logger.WithError(err).Error("fail to insert locked placeholder")
			tx.Rollback()
			return err
		}
		if err = tx.Commit(); err != nil {
			logger.WithError(err).Error("fail to commit")
			tx.Rollback()
			return err
		}
		tx.Rollback()
		return nil
	}
	if locked {
		logger.WithError(err).Error("state already locked")
		tx.Rollback()
		return errors.New("already locked")
	}
	err = storage.transactionLockState(tx, sessionID, owner, stateName)
	if err != nil {
		logger.WithError(err).Error("fail to lock state")
		tx.Rollback()
		return err
	}
	err = tx.Commit()
	if err != nil {
		logger.WithError(err).Error("fail to commit")
		tx.Rollback()
		return err
	}
	return nil
}

func (storage TFStateStorageSqlite) transactionLookupState(tx *sql.Tx, owner, stateName string) (exists bool, locked bool, sessionID string, err error) {
	row := tx.QueryRow("SELECT lock, session_id FROM tf_state WHERE owner = ? AND state_name = ?", owner, stateName)
	var fetchedSessionID string
	err = row.Scan(&locked, &fetchedSessionID)
	if errors.Is(err, sql.ErrNoRows) {
		return false, false, "", nil
	} else if err != nil {
		//logger.WithError(err).Error("fail to scan row")
		return false, false, "", err
	}
	return true, locked, fetchedSessionID, nil
}

func (storage TFStateStorageSqlite) transactionInsertLockedPlaceHolder(tx *sql.Tx, sessionID, owner, stateName string) error {
	sqlStmt := `INSERT INTO tf_state (owner, state_name, state, lock, session_id, created_at, updated_at) VALUES (?, ?, ?, ?, ?, ?, NULL);`
	_, err := tx.Exec(sqlStmt, owner, stateName, nil, true, sessionID, storage.timeNow())
	if err != nil {
		return err
	}
	return nil
}

func (storage TFStateStorageSqlite) transactionLockState(tx *sql.Tx, sessionID, owner, stateName string) error {
	_, err := tx.Exec("UPDATE tf_state SET lock = 1, session_id = ? WHERE owner = ? AND state_name = ? AND lock = 0", sessionID, owner, stateName)
	if err != nil {
		//logger.WithError(err).Error("fail to lock state")
		tx.Rollback()
		return err
	}
	return nil
}

// Unlock ...
func (storage TFStateStorageSqlite) Unlock(sessionID, owner, stateName string) error {
	logger := log.WithFields(log.Fields{
		"package":   "adapters",
		"function":  "TFStateStorageSqlite.Unlock",
		"owner":     owner,
		"stateName": stateName,
	})
	results, err := storage.db.Exec("UPDATE tf_state SET lock = 0 WHERE owner = ? AND state_name = ? AND session_id = ?", owner, stateName, sessionID)
	if err != nil {
		logger.WithError(err).Error("fail to unlock")
		return err
	}
	affected, err := results.RowsAffected()
	if err != nil {
		logger.WithError(err).Error("fail to obtain rows affected")
		return err
	}
	if affected == 0 {
		err = fmt.Errorf("state name didn't exist or %w", types.ErrTFStateLockedByOthers)
		logger.WithError(err).Error()
		return err
	}
	return nil
}

// Locked checks if a state is locked or not, return true if locked.
func (storage TFStateStorageSqlite) Locked(owner, stateName string) (bool, error) {
	logger := log.WithFields(log.Fields{
		"package":   "adapters",
		"function":  "TFStateStorageSqlite.Locked",
		"owner":     owner,
		"stateName": stateName,
	})
	row := storage.db.QueryRow("SELECT lock, session_id FROM tf_state WHERE owner = ? AND state_name = ?", owner, stateName)
	var result struct {
		Lock      bool
		SessionID string
	}
	err := row.Scan(&result.Lock, &result.SessionID)
	if errors.Is(err, sql.ErrNoRows) {
		return false, types.ErrTFStateNotFound
	} else if err != nil {
		logger.WithError(err).Error()
		return false, err
	}
	return result.Lock, nil
}

// Close ...
func (storage TFStateStorageSqlite) Close() error {
	return storage.db.Close()
}

func (storage TFStateStorageSqlite) timeNow() time.Time {
	return time.Now().UTC()
}

// time is serialized as string to store in sqlite
func (storage TFStateStorageSqlite) serializeTime(t time.Time) (string, error) {
	marshal, err := t.MarshalText()
	if err != nil {
		return "", err
	}
	return string(marshal), nil
}

func (storage TFStateStorageSqlite) deserializeTime(timeStr string) (time.Time, error) {
	var t time.Time
	err := t.UnmarshalText([]byte(timeStr))
	if err != nil {
		return time.Time{}, err
	}
	return t, nil
}
