package adapters

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/terraform-http-backend/types"
	"testing"
	"time"
)

// TFStateStorageCommon is a common interface for Terraform state storage.
// The purpose of this interface is to share tests between unit test (sqlite) and integration test (psql).
// This interface contains more methods than ports.StateStorage, since ports.StateStorage only contains the methods that domain uses.
type TFStateStorageCommon interface {
	Lookup(owner, stateName string) bool
	Fetch(owner, stateName string) (*types.TerraformState, error)
	List(owner string) ([]string, error)
	ListHistories(owner, stateName string) ([]types.TerraformStateHistory, error)
	Create(sessionID, owner, stateName string, state types.TerraformState, opts ...types.StateCreateOption) (*types.TerraformState, error)
	UpdateOrCreate(sessionID, owner, stateName string, state types.TerraformState) (created bool, err error)
	UpdateExisting(sessionID, owner, stateName string, state types.TerraformState) (*types.TerraformState, error)
	Delete(sessionID, owner, stateName string) error
	DeleteHistories(owner, stateName string) (bool, error)
	Lock(sessionID, owner, stateName string) error
	Unlock(sessionID, owner, stateName string) error
	Locked(owner, stateName string) (bool, error)
	Close() error
}

// TestTFStateStorage is a set of common tests for all implementation of TFStateStorageCommon (sqlite, psql)
func TestTFStateStorage(t *testing.T, setup func() TFStateStorageCommon) {
	t.Run("lookup empty", func(t *testing.T) {
		storage := setup()
		assert.False(t, storage.Lookup("not-exist", "not-exist"))
	})
	t.Run("create", func(t *testing.T) {
		storage := setup()
		state := types.TerraformState{Version: 123, TFVersion: "123"}
		createdState, err := storage.Create("session-123", "foo", "bar", state)
		assert.NoError(t, err)
		assert.NotNil(t, createdState)
		assert.Equal(t, state, *createdState)
		assert.True(t, storage.Lookup("foo", "bar"))
		fetched, err := storage.Fetch("foo", "bar")
		assert.NoError(t, err)
		assert.NotNil(t, fetched)
		assert.Equal(t, state, *fetched)
	})

	t.Run("create already exist", func(t *testing.T) {
		storage := setup()
		state := types.TerraformState{Version: 123, TFVersion: "123"}
		createdState, err := storage.Create("session-123", "foo", "bar", state)
		assert.NoError(t, err)
		assert.NotNil(t, createdState)
		assert.Equal(t, state, *createdState)

		_, err = storage.Create("session-123", "foo", "bar", state)
		assert.Error(t, err)
	})

	t.Run("update non-existent", func(t *testing.T) {
		storage := setup()

		state := types.TerraformState{Version: 123, TFVersion: "123"}
		_, err := storage.UpdateExisting("session-123", "foo", "bar", state)
		assert.Error(t, err)
	})

	t.Run("update existing", func(t *testing.T) {
		storage := setup()

		state := types.TerraformState{Version: 123, TFVersion: "123"}
		_, err := storage.Create("session-123", "foo", "bar", state)
		assert.NoError(t, err)
		_, err = storage.UpdateExisting("session-123", "foo", "bar", state)
		assert.NoError(t, err)
		fetched, err := storage.Fetch("foo", "bar")
		assert.NoError(t, err)
		assert.NotNil(t, fetched)
		assert.Equal(t, state, *fetched)
	})

	t.Run("update existing w/ bad session ID", func(t *testing.T) {
		storage := setup()
		state1 := types.TerraformState{Version: 123, TFVersion: "123"}
		_, err := storage.Create("session-123", "foo", "bar", state1)
		assert.NoError(t, err)
		err = storage.Lock("session-123", "foo", "bar")
		assert.NoError(t, err)
		state2 := state1
		state2.TFVersion = "234"
		_, err = storage.UpdateExisting("session-123-bad", "foo", "bar", state2)
		assert.Error(t, err)
		fetched, err := storage.Fetch("foo", "bar")
		assert.NoError(t, err)
		assert.NotNil(t, fetched)
		assert.Equal(t, state1, *fetched)
	})

	t.Run("update or create", func(t *testing.T) {
		storage := setup()
		state := types.TerraformState{Version: 123, TFVersion: "123"}
		created, err := storage.UpdateOrCreate("session-123", "foo", "bar", state)
		assert.NoError(t, err)
		assert.True(t, created)

		assert.True(t, storage.Lookup("foo", "bar"))
		fetched, err := storage.Fetch("foo", "bar")
		assert.NoError(t, err)
		assert.NotNil(t, fetched)
		assert.Equal(t, state, *fetched)
	})

	t.Run("update or create again", func(t *testing.T) {
		storage := setup()
		state := types.TerraformState{Version: 123, TFVersion: "123"}
		created, err := storage.UpdateOrCreate("session-123", "foo", "bar", state)
		assert.NoError(t, err)
		assert.True(t, created)

		state.TFVersion = "321"
		created, err = storage.UpdateOrCreate("session-123", "foo", "bar", state)
		assert.NoError(t, err)
		assert.False(t, created)

		fetched, err := storage.Fetch("foo", "bar")
		assert.NoError(t, err)
		assert.NotNil(t, fetched)
		assert.Equal(t, state, *fetched)
	})

	t.Run("update or create again w/ bad session ID", func(t *testing.T) {
		storage := setup()
		state1 := types.TerraformState{Version: 123, TFVersion: "123"}
		created, err := storage.UpdateOrCreate("session-123", "foo", "bar", state1)
		assert.NoError(t, err)
		assert.True(t, created)
		err = storage.Lock("session-123", "foo", "bar")
		assert.NoError(t, err)

		state2 := state1
		state2.TFVersion = "321"
		_, err = storage.UpdateOrCreate("session-123-bad", "foo", "bar", state2)
		assert.Error(t, err)

		fetched, err := storage.Fetch("foo", "bar")
		assert.NoError(t, err)
		assert.NotNil(t, fetched)
		assert.Equal(t, state1, *fetched)
	})

	t.Run("delete", func(t *testing.T) {
		storage := setup()
		state := types.TerraformState{Version: 123, TFVersion: "123"}
		_, err := storage.Create("session-123", "foo", "bar", state)
		assert.NoError(t, err)
		err = storage.Delete("session-123", "foo", "bar")
		assert.NoError(t, err)
		assert.False(t, storage.Lookup("foo", "bar"))
	})

	t.Run("delete w/ bad session ID", func(t *testing.T) {
		storage := setup()
		state := types.TerraformState{Version: 123, TFVersion: "123"}
		_, err := storage.Create("session-123", "foo", "bar", state)
		assert.NoError(t, err)
		err = storage.Lock("session-123", "foo", "bar")
		assert.NoError(t, err)
		err = storage.Delete("session-123-bad", "foo", "bar")
		assert.Error(t, err)
		assert.True(t, storage.Lookup("foo", "bar"))
	})

	t.Run("lock", func(t *testing.T) {
		storage := setup()
		state := types.TerraformState{Version: 123, TFVersion: "123"}
		_, err := storage.Create("session-123", "foo", "bar", state)
		assert.NoError(t, err)
		err = storage.Lock("session-123", "foo", "bar")
		assert.NoError(t, err)
		locked, err := storage.Locked("foo", "bar")
		assert.NoError(t, err)
		assert.True(t, locked)
	})

	t.Run("lock non-existent", func(t *testing.T) {
		// lock on non-existent state creates a placeholder
		storage := setup()
		err := storage.Lock("session-123", "foo", "bar")
		assert.NoError(t, err)
		_, err = storage.Locked("foo", "bar")
		assert.NoError(t, err)
		assert.True(t, storage.Lookup("foo", "bar"))
		fetchedState, err := storage.Fetch("foo", "bar")
		assert.NoError(t, err)
		assert.Nil(t, fetchedState)
	})

	t.Run("lock again", func(t *testing.T) {
		storage := setup()
		state := types.TerraformState{Version: 123, TFVersion: "123"}
		_, err := storage.Create("session-123", "foo", "bar", state)
		assert.NoError(t, err)
		err = storage.Lock("session-123", "foo", "bar")
		assert.NoError(t, err)
		err = storage.Lock("session-123", "foo", "bar")
		assert.Error(t, err)
		locked, err := storage.Locked("foo", "bar")
		assert.NoError(t, err)
		assert.True(t, locked)
	})

	t.Run("unlock", func(t *testing.T) {
		storage := setup()
		state := types.TerraformState{Version: 123, TFVersion: "123"}
		_, err := storage.Create("session-123", "foo", "bar", state)
		assert.NoError(t, err)
		err = storage.Lock("session-123", "foo", "bar")
		assert.NoError(t, err)
		err = storage.Unlock("session-123", "foo", "bar")
		assert.NoError(t, err)
		locked, err := storage.Locked("foo", "bar")
		assert.NoError(t, err)
		assert.False(t, locked)
	})

	t.Run("unlock non-existent", func(t *testing.T) {
		storage := setup()
		err := storage.Unlock("session-123", "foo", "bar")
		assert.Error(t, err)
		_, err = storage.Locked("foo", "bar")
		assert.Error(t, err)
	})

	t.Run("unlock the unlocked", func(t *testing.T) {
		// different behavior than mem
		storage := setup()
		state := types.TerraformState{Version: 123, TFVersion: "123"}
		_, err := storage.Create("session-123", "foo", "bar", state)
		assert.NoError(t, err)
		err = storage.Unlock("session-123", "foo", "bar")
		assert.Error(t, err)
		locked, err := storage.Locked("foo", "bar")
		assert.NoError(t, err)
		assert.False(t, locked)
	})

	t.Run("unlock bad session id", func(t *testing.T) {
		storage := setup()
		state := types.TerraformState{Version: 123, TFVersion: "123"}
		_, err := storage.Create("session-123", "foo", "bar", state)
		assert.NoError(t, err)
		err = storage.Lock("session-123", "foo", "bar")
		assert.NoError(t, err)
		err = storage.Unlock("session-123-bad", "foo", "bar")
		assert.Error(t, err)
		locked, err := storage.Locked("foo", "bar")
		assert.NoError(t, err)
		assert.True(t, locked)
	})

	t.Run("list no history", func(t *testing.T) {
		storage := setup()
		state := types.TerraformState{Version: 123, TFVersion: "123"}
		_, err := storage.Create("session-123", "foo", "bar", state)
		assert.NoError(t, err)
		histories, err := storage.ListHistories("foo", "bar")
		assert.NoError(t, err)
		assert.NotNil(t, histories)
		assert.Len(t, histories, 0)
	})

	t.Run("list 1 history", func(t *testing.T) {
		storage := setup()
		state1 := types.TerraformState{Version: 123, TFVersion: "ver1"}
		_, err := storage.Create("session-123", "foo", "bar", state1)
		assert.NoError(t, err)
		state2 := state1
		state2.TFVersion = "ver2"
		beforeUpdate := time.Now()
		_, err = storage.UpdateExisting("session-123", "foo", "bar", state2)
		assert.NoError(t, err)
		histories, err := storage.ListHistories("foo", "bar")
		assert.NoError(t, err)
		assert.NotNil(t, histories)
		if !assert.Len(t, histories, 1) {
			return
		}
		assert.NotNil(t, histories[0].State)
		assert.Equal(t, state1, *histories[0].State)
		assert.True(t, histories[0].CreatedAt.After(beforeUpdate))
	})

	t.Run("list 2 history", func(t *testing.T) {
		storage := setup()
		state1 := types.TerraformState{Version: 123, TFVersion: "ver1"}
		_, err := storage.Create("session-123", "foo", "bar", state1)
		assert.NoError(t, err)

		state2 := state1
		state2.TFVersion = "ver2"
		beforeUpdate := time.Now()
		_, err = storage.UpdateExisting("session-123", "foo", "bar", state2)
		assert.NoError(t, err)
		histories, err := storage.ListHistories("foo", "bar")
		assert.NoError(t, err)
		assert.NotNil(t, histories)
		if !assert.Len(t, histories, 1) {
			return
		}
		assert.NotNil(t, histories[0].State)
		assert.Equal(t, state1, *histories[0].State)
		assert.True(t, histories[0].CreatedAt.After(beforeUpdate))

		state3 := state2
		state3.TFVersion = "ver3"
		beforeUpdate = time.Now()
		_, err = storage.UpdateExisting("session-123", "foo", "bar", state3)
		assert.NoError(t, err)
		histories, err = storage.ListHistories("foo", "bar")
		assert.NoError(t, err)
		assert.NotNil(t, histories)
		if !assert.Len(t, histories, 2) {
			return
		}
		assert.NotNil(t, histories[1].State)
		assert.Equal(t, state2, *histories[1].State)
		assert.True(t, histories[1].CreatedAt.After(beforeUpdate))
	})

	t.Run("list N history", func(t *testing.T) {
		storage := setup()
		originalState := types.TerraformState{Version: 123, TFVersion: "ver?"}
		_, err := storage.Create("session-123", "foo", "bar", originalState)
		assert.NoError(t, err)
		oldState := originalState

		for i := 0; i < 3; i++ {
			newState := oldState
			newState.TFVersion = fmt.Sprintf("ver%d", i)
			beforeUpdate := time.Now()
			_, err = storage.UpdateExisting("session-123", "foo", "bar", newState)
			assert.NoError(t, err)
			histories, err := storage.ListHistories("foo", "bar")
			assert.NoError(t, err)
			assert.NotNil(t, histories)
			if !assert.Len(t, histories, i+1) {
				return
			}
			mostRecent := findLastHistory(histories)
			assert.NotNil(t, mostRecent)
			assert.NotNil(t, oldState, mostRecent.State)
			assert.Equal(t, oldState, *mostRecent.State)
			assert.True(t, mostRecent.CreatedAt.After(beforeUpdate))
			oldState = newState
		}
	})

	t.Run("delete history", func(t *testing.T) {
		storage := setup()
		state := types.TerraformState{Version: 123, TFVersion: "ver1"}
		_, err := storage.Create("session-123", "foo", "bar", state)
		assert.NoError(t, err)
		state.TFVersion = "ver2"
		_, err = storage.UpdateExisting("session-123", "foo", "bar", state)
		assert.NoError(t, err)
		histories, err := storage.ListHistories("foo", "bar")
		assert.NoError(t, err)
		assert.NotNil(t, histories)
		assert.Len(t, histories, 1)
		deleted, err := storage.DeleteHistories("foo", "bar")
		assert.NoError(t, err)
		assert.True(t, deleted)
		histories, err = storage.ListHistories("foo", "bar")
		assert.NoError(t, err)
		assert.NotNil(t, histories)
		assert.Len(t, histories, 0)
	})

	t.Run("delete history with no history", func(t *testing.T) {
		storage := setup()
		state := types.TerraformState{Version: 123, TFVersion: "ver1"}
		_, err := storage.Create("session-123", "foo", "bar", state)
		assert.NoError(t, err)
		histories, err := storage.ListHistories("foo", "bar")
		assert.NoError(t, err)
		assert.NotNil(t, histories)
		assert.Len(t, histories, 0)
		deleted, err := storage.DeleteHistories("foo", "bar")
		assert.NoError(t, err)
		assert.False(t, deleted)
		histories, err = storage.ListHistories("foo", "bar")
		assert.NoError(t, err)
		assert.NotNil(t, histories)
		assert.Len(t, histories, 0)
	})

	t.Run("delete state but not history", func(t *testing.T) {
		storage := setup()
		state := types.TerraformState{Version: 123, TFVersion: "ver1"}
		_, err := storage.Create("session-123", "foo", "bar", state)
		assert.NoError(t, err)
		state.TFVersion = "ver2"
		_, err = storage.UpdateExisting("session-123", "foo", "bar", state)
		assert.NoError(t, err)
		histories, err := storage.ListHistories("foo", "bar")
		assert.NoError(t, err)
		assert.NotNil(t, histories)
		assert.Len(t, histories, 1)
		err = storage.Delete("session-123", "foo", "bar")
		assert.NoError(t, err)
		histories, err = storage.ListHistories("foo", "bar")
		assert.NoError(t, err)
		assert.NotNil(t, histories)
		assert.Len(t, histories, 2) // with Delete(), last state becomes a history
	})

	t.Run("overwrite histories with create", func(t *testing.T) {
		storage := setup()
		state := types.TerraformState{Version: 123, TFVersion: "ver1"}
		_, err := storage.Create("session-123", "foo", "bar", state)
		assert.NoError(t, err)
		state.TFVersion = "ver2"
		_, err = storage.UpdateExisting("session-123", "foo", "bar", state)
		assert.NoError(t, err)
		histories, err := storage.ListHistories("foo", "bar")
		assert.NoError(t, err)
		assert.NotNil(t, histories)
		assert.Len(t, histories, 1)
		err = storage.Delete("session-123", "foo", "bar")
		assert.NoError(t, err)
		_, err = storage.Create("session-123", "foo", "bar", state)
		assert.NoError(t, err)
		histories, err = storage.ListHistories("foo", "bar")
		assert.NoError(t, err)
		assert.NotNil(t, histories)
		assert.Len(t, histories, 0) // create overwrite the histories of previous state.
	})
}
