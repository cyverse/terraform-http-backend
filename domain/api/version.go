package api

import (
	"net/http"

	"github.com/gorilla/mux"
)

// VersionAPI ...
type VersionAPI struct {
	Version string
}

// Router register version endpoint
func (api VersionAPI) Router(r *mux.Router, pathPrefix string) {
	r.HandleFunc(pathPrefix+"/version", api.GetVersion).Methods(http.MethodGet)
}

// GetVersion ...
func (api VersionAPI) GetVersion(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	if api.Version == "" {
		w.Write([]byte("unknown version"))
	} else {
		w.Write([]byte(api.Version))
	}
}
