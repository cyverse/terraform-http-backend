package tfbackend

import (
	"errors"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/terraform-http-backend/adapters"
	"gitlab.com/cyverse/terraform-http-backend/types"
	"strconv"
	"testing"
	"time"
)

func TestTFBackend(t *testing.T) {
	//log.SetLevel(log.DebugLevel)
	t.Run("get non-existent", func(t *testing.T) {
		storage := adapters.NewTFStateStorageSqliteMem()
		backend := TFBackend{storage}
		owner := "foo"
		stateName := "bar"
		state, err := backend.GetState(owner, stateName)
		if assert.Error(t, err) {
			assert.True(t, errors.Is(err, types.ErrTFStateNotFound))
		}
		assert.Nil(t, state)
	})
	t.Run("create", func(t *testing.T) {
		storage := adapters.NewTFStateStorageSqliteMem()
		backend := TFBackend{storage}
		sessionID := "session-123"
		owner := "foo"
		stateName := "bar"
		stateStr := "{}"
		created, err := backend.UpdateOrCreateState(sessionID, owner, stateName, []byte(stateStr))
		assert.NoError(t, err)
		assert.True(t, created)
		state, err := backend.GetState(owner, stateName)
		assert.NoError(t, err)
		if !assert.NotNil(t, state) {
			return
		}
		assert.Equal(t, *state, types.TerraformState{})
	})
	t.Run("update", func(t *testing.T) {
		storage := adapters.NewTFStateStorageSqliteMem()
		backend := TFBackend{storage}
		sessionID := "session-123"
		owner := "foo"
		stateName := "bar"
		stateStr := "{}"
		created, err := backend.UpdateOrCreateState(sessionID, owner, stateName, []byte(stateStr))
		assert.NoError(t, err)
		assert.True(t, created)
		created, err = backend.UpdateOrCreateState(sessionID, owner, stateName, []byte(stateStr))
		assert.NoError(t, err)
		assert.False(t, created)
		state, err := backend.GetState(owner, stateName)
		assert.NoError(t, err)
		if !assert.NotNil(t, state) {
			return
		}
		assert.Equal(t, *state, types.TerraformState{})
	})
	t.Run("update unlocked w/ diff session", func(t *testing.T) {
		// if state is not locked, it can be updated by any session id
		storage := adapters.NewTFStateStorageSqliteMem()
		backend := TFBackend{storage}
		sessionID := "session-123"
		owner := "foo"
		stateName := "bar"
		stateStr := "{}"
		created, err := backend.UpdateOrCreateState(sessionID, owner, stateName, []byte(stateStr))
		assert.NoError(t, err)
		assert.True(t, created)
		created, err = backend.UpdateOrCreateState(sessionID+"-diff", owner, stateName, []byte(stateStr))
		assert.NoError(t, err)
		assert.False(t, created)
	})
	t.Run("delete", func(t *testing.T) {
		storage := adapters.NewTFStateStorageSqliteMem()
		backend := TFBackend{storage}
		sessionID := "session-123"
		owner := "foo"
		stateName := "bar"
		stateStr := "{}"
		created, err := backend.UpdateOrCreateState(sessionID, owner, stateName, []byte(stateStr))
		assert.NoError(t, err)
		assert.True(t, created)
		err = backend.DeleteState(sessionID, owner, stateName)
		assert.NoError(t, err)
		tfState, err := backend.GetState(owner, stateName)
		assert.Error(t, err)
		assert.Nil(t, tfState)
	})
	t.Run("lock non-existent", func(t *testing.T) {
		storage := adapters.NewTFStateStorageSqliteMem()
		backend := TFBackend{storage}
		sessionID := "session-123"
		owner := "foo"
		stateName := "bar"
		err := backend.LockState(sessionID, owner, stateName)
		assert.NoError(t, err)
		tfState, err := backend.GetState(owner, stateName)
		assert.NoError(t, err)
		assert.Nil(t, tfState)
	})
	t.Run("lock, create, unlock", func(t *testing.T) {
		storage := adapters.NewTFStateStorageSqliteMem()
		backend := TFBackend{storage}
		sessionID := "session-123"
		owner := "foo"
		stateName := "bar"
		err := backend.LockState(sessionID, owner, stateName)
		assert.NoError(t, err)
		stateStr := "{}"
		created, err := backend.UpdateOrCreateState(sessionID, owner, stateName, []byte(stateStr))
		assert.NoError(t, err)
		assert.False(t, created) // lock created a placeholder, thus the state already exists
		err = backend.UnlockState(sessionID, owner, stateName)
		assert.NoError(t, err)
	})
	t.Run("lock, create w/ diff session, unlock", func(t *testing.T) {
		storage := adapters.NewTFStateStorageSqliteMem()
		backend := TFBackend{storage}
		sessionID := "session-123"
		owner := "foo"
		stateName := "bar"
		stateStr := "{}"
		err := backend.LockState(sessionID, owner, stateName)
		assert.NoError(t, err)
		_, err = backend.UpdateOrCreateState(sessionID+"-diff", owner, stateName, []byte(stateStr))
		assert.Error(t, err)
		err = backend.UnlockState(sessionID, owner, stateName)
		assert.NoError(t, err)
	})
	t.Run("list states, empty", func(t *testing.T) {
		storage := adapters.NewTFStateStorageSqliteMem()
		backend := TFBackend{storage}
		owner := "foo"
		states, err := backend.ListStates(owner)
		assert.NoError(t, err)
		assert.NotNil(t, states)
		assert.Empty(t, states)
	})
	t.Run("list states, 1", func(t *testing.T) {
		storage := adapters.NewTFStateStorageSqliteMem()
		backend := TFBackend{storage}
		sessionID := "session-123"
		owner := "foo"
		stateName := "bar"
		stateStr := "{}"
		created, err := backend.UpdateOrCreateState(sessionID, owner, stateName, []byte(stateStr))
		assert.NoError(t, err)
		assert.True(t, created)
		states, err := backend.ListStates(owner)
		assert.NoError(t, err)
		assert.NotNil(t, states)
		assert.Len(t, states, 1)
	})
	t.Run("list states, N", func(t *testing.T) {
		storage := adapters.NewTFStateStorageSqliteMem()
		backend := TFBackend{storage}
		sessionID := "session-123"
		owner := "foo"
		stateStr := "{}"
		const count = 100
		for i := 0; i < count; i++ {
			stateName := strconv.Itoa(i)
			created, err := backend.UpdateOrCreateState(sessionID, owner, stateName, []byte(stateStr))
			assert.NoError(t, err)
			assert.True(t, created)
		}
		states, err := backend.ListStates(owner)
		assert.NoError(t, err)
		assert.NotNil(t, states)
		assert.Len(t, states, count)
	})
	t.Run("list state hist, empty", func(t *testing.T) {
		storage := adapters.NewTFStateStorageSqliteMem()
		backend := TFBackend{storage}
		sessionID := "session-123"
		owner := "foo"
		stateName := "bar"
		stateStr := "{}"
		created, err := backend.UpdateOrCreateState(sessionID, owner, stateName, []byte(stateStr))
		assert.NoError(t, err)
		assert.True(t, created)
		histories, err := backend.ListStateHistories(owner, stateName)
		assert.NoError(t, err)
		assert.NotNil(t, histories)
		assert.Len(t, histories, 0)
	})
	t.Run("list state hist, 1", func(t *testing.T) {
		storage := adapters.NewTFStateStorageSqliteMem()
		backend := TFBackend{storage}
		sessionID := "session-123"
		owner := "foo"
		stateName := "bar"
		stateStr := `{"resources":[]}`
		created, err := backend.UpdateOrCreateState(sessionID, owner, stateName, []byte(stateStr))
		assert.NoError(t, err)
		assert.True(t, created)
		created, err = backend.UpdateOrCreateState(sessionID, owner, stateName, []byte(stateStr))
		assert.NoError(t, err)
		assert.False(t, created)
		histories, err := backend.ListStateHistories(owner, stateName)
		assert.NoError(t, err)
		assert.NotNil(t, histories)
		if assert.Len(t, histories, 1) {
			assert.NotEqual(t, time.Time{}, histories[0].CreatedAt)
			assert.Equal(t, &types.TerraformState{
				Resources: []types.TerraformResource{},
			}, histories[0].State)
		}
	})
	t.Run("delete state will create history entry", func(t *testing.T) {
		storage := adapters.NewTFStateStorageSqliteMem()
		backend := TFBackend{storage}
		sessionID := "session-123"
		owner := "foo"
		stateName := "bar"
		stateStr := `{"resources":[]}`
		created, err := backend.UpdateOrCreateState(sessionID, owner, stateName, []byte(stateStr))
		assert.NoError(t, err)
		assert.True(t, created)

		histories, err := backend.ListStateHistories(owner, stateName)
		assert.NoError(t, err)
		assert.NotNil(t, histories)
		assert.Len(t, histories, 0)

		err = backend.DeleteState(sessionID, owner, stateName)
		assert.NoError(t, err)
		histories, err = backend.ListStateHistories(owner, stateName)
		assert.NoError(t, err)
		assert.NotNil(t, histories)
		if assert.Len(t, histories, 1) {
			assert.NotEqual(t, time.Time{}, histories[0].CreatedAt)
			assert.Equal(t, &types.TerraformState{
				Resources: []types.TerraformResource{},
			}, histories[0].State)
		}
	})
}
