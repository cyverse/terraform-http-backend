package tfbackend

import (
	"encoding/json"
	"gitlab.com/cyverse/terraform-http-backend/ports"
	"gitlab.com/cyverse/terraform-http-backend/types"
)

// TerraformBackend ...
type TerraformBackend interface {
	GetState(owner, stateName string) (*types.TerraformState, error)
	ListStates(owner string) ([]string, error)
	ListStateHistories(owner, stateName string) ([]types.TerraformStateHistory, error)
	// UpdateOrCreateState return true if state already exists, false if it is newly created
	UpdateOrCreateState(sessionID, owner, stateName string, tfStateStr []byte) (bool, error)
	DeleteState(sessionID, owner, stateName string) error
	LockState(sessionID, owner, stateName string) error
	UnlockState(sessionID, owner, stateName string) error
}

// TFBackend is a implementation of TerraformBackend
type TFBackend struct {
	stateStorage ports.StateStorage
}

// NewTFBackend ...
func NewTFBackend(stateStorage ports.StateStorage) *TFBackend {
	return &TFBackend{stateStorage: stateStorage}
}

// GetState ...
func (tfb TFBackend) GetState(owner, stateName string) (*types.TerraformState, error) {

	tfState, err := tfb.stateStorage.Fetch(owner, stateName)
	if err != nil {
		return nil, err
	}
	if tfState == nil {
		return nil, nil
	}

	_, err = json.Marshal(tfState)
	if err != nil {
		return nil, err
	}
	return tfState, nil
}

// ListStates ...
func (tfb TFBackend) ListStates(owner string) ([]string, error) {
	return tfb.stateStorage.List(owner)
}

// ListStateHistories ...
func (tfb TFBackend) ListStateHistories(owner, stateName string) ([]types.TerraformStateHistory, error) {
	histories, err := tfb.stateStorage.ListHistories(owner, stateName)
	if err != nil {
		return nil, err
	}
	return histories, nil
}

// UpdateOrCreateState ...
func (tfb TFBackend) UpdateOrCreateState(sessionID, owner, stateName string, tfStateStr []byte) (bool, error) {
	var tfState types.TerraformState
	err := json.Unmarshal(tfStateStr, &tfState)
	if err != nil {
		return false, err
	}

	created, err := tfb.stateStorage.UpdateOrCreate(sessionID, owner, stateName, tfState)
	if err != nil {
		return false, err
	}
	return created, nil
}

// DeleteState ...
func (tfb TFBackend) DeleteState(sessionID, owner, stateName string) error {
	return tfb.stateStorage.Delete(sessionID, owner, stateName)
}

// LockState ...
func (tfb TFBackend) LockState(sessionID, owner, stateName string) error {
	return tfb.stateStorage.Lock(sessionID, owner, stateName)
}

// UnlockState ...
func (tfb TFBackend) UnlockState(sessionID, owner, stateName string) error {
	return tfb.stateStorage.Unlock(sessionID, owner, stateName)
}
