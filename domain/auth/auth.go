package auth

import "net/http"

// Driver authentication driver
type Driver interface {
	Middleware(next http.Handler) http.Handler
}

// RequestContextKey is the key for context key-value of an HTTP request.
type RequestContextKey int

const (
	// RequestCtxUsername is the key for extracting username from HTTP request's context.
	RequestCtxUsername RequestContextKey = iota + 1
)
