package auth

import (
	"crypto/rand"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func Test_validateToken(t *testing.T) {
	key := make([]byte, 50)
	rand.Read(key)
	username := "testuser123"
	t.Run("RegisteredClaims", func(t *testing.T) {
		token, err := GenerateNewTokenWithRegisteredClaims(username, time.Hour, func() []byte {
			return key
		})
		if !assert.NoError(t, err) {
			return
		}
		assert.NotEmpty(t, token)
		var auth = jwtAuthBase{
			keySrc: func() []byte {
				return key
			},
		}
		extractedUsername, err := auth.validateToken(token)
		if !assert.NoError(t, err) {
			return
		}
		assert.NotEmpty(t, extractedUsername)
		assert.Equal(t, username, extractedUsername)
	})
}
