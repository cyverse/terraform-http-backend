# Terraform HTTP Backend
Backend for storing Terraform state.
https://www.terraform.io/language/settings/backends/http

# Environment variables
| name | description | default |
| ---- | ----------- | ------- | 
| POD_NAME | k8s pod name |  |
| LOG_LEVEL | log level (`info`/`debug`/`trace`) | trace |
| HTTP_PORT | port to listen on | 8080 |
| URL_PREFIX | prefix to the base URL |  |
| JWT_SECRET | secret key for verify JWT token in request header |  |
| STATE_STORAGE | `postgres` or `sqlite` | sqlite |
| SQLITE_FILE | filename of the sqlite database | tf_backend.db |
| PSQL_HOST | postgres hostname | |
| PSQL_PORT | postgres port | |
| PSQL_DB | postgres database | |
| PSQL_USERNAME | postgres username | |
| PSQL_PASSWORD | postgres password | |

# Concepts
## State
A json blob that represents all resources provisoned by terraform.

## State Name
A user can have multiple state stored in the backend.
Each state will have a name that is part of the URL.

## State History
State history is created when:
- a state is updated
- a state is deleted

State history of a state will be cleared when:
- the state history is explicitly cleared
- the state is deleted, and then re-created.

## Lock
A state can be locked with a session ID to ensure only 1 terraform instance can modify it.
When locking a state that does not exist, a new state record will be created,
the Terraform state of the record will be NULL.
