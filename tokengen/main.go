package main

import (
	"fmt"
	"os"
	"strconv"
	"time"

	"gitlab.com/cyverse/terraform-http-backend/domain/auth"
)

const defaultTokenLifetime = time.Minute * 30

func main() {
	username, lifetime := parseArgs()
	token, err := auth.GenerateNewTokenWithRegisteredClaims(username, lifetime, getKeyFromEnv)
	if err != nil {
		printErrAndExit(err)
	}
	fmt.Println(token)
}

func parseArgs() (username string, lifetime time.Duration) {
	if len(os.Args) <= 1 {
		printHelp()
		printErrAndExit("missing username")
	}
	username = os.Args[1]
	lifetime = defaultTokenLifetime
	if len(os.Args) >= 3 {
		lifetimeStr := os.Args[2]
		parseUint, err := strconv.ParseUint(lifetimeStr[:len(lifetimeStr)-1], 10, 32)
		if err != nil {
			printHelp()
			printErrAndExit(err)
		}
		switch lifetimeStr[len(lifetimeStr)-1] {
		case 'd':
			// day
			lifetime = time.Hour * 24 * time.Duration(parseUint)
		case 'h':
			// hour
			lifetime = time.Hour * time.Duration(parseUint)
		case 'm':
			// minute
			lifetime = time.Minute * time.Duration(parseUint)
		case 's':
			// second
			lifetime = time.Second * time.Duration(parseUint)
		default:
			printHelp()
			printErrAndExit("unrecognizable time unit")
		}
	}
	return username, lifetime
}

func getKeyFromEnv() []byte {
	key, ok := os.LookupEnv("JWT_SECRET")
	if !ok {
		fmt.Fprintln(os.Stderr, "env missing JWT_SECRET")
		os.Exit(1)
	}
	if len(key) < 32 {
		fmt.Fprintln(os.Stderr, "key length < 32")
		os.Exit(1)
	}
	return []byte(key)
}

func printErrAndExit(a ...interface{}) {
	fmt.Fprintln(os.Stderr, a...)
	os.Exit(1)
}

func printHelp() {
	fmt.Println(`tokengen [username] [lifetime]
	lifetime can be ?d (? days), ?h (? hours), ?m (? minutes), ?s (? seconds).`)
}
