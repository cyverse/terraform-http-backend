// integration test
package tests

import (
	"github.com/kelseyhightower/envconfig"
	"os"
	"testing"
	// postgresql driver
	_ "github.com/jackc/pgx/v5/stdlib"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/terraform-http-backend/adapters"
	"gitlab.com/cyverse/terraform-http-backend/types"
)

var testPsqlConf types.PostgresqlConfig

func init() {
	if err := envconfig.Process("", &testPsqlConf); err != nil {
		log.WithError(err).Fatal("fail to load psql config from env var")
	}
}

func TestTFStateStoragePostgres(t *testing.T) {
	if os.Getenv("CI_INTEGRATION") != "true" {
		t.Skip("Skipping testing in non-CI environment")
		return
	}
	adapters.TestTFStateStorage(t, func() adapters.TFStateStorageCommon {
		return testSetup()
	})
}

// drop 2 tables before each test setup
func testSetup() *adapters.TFStateStoragePostgres {
	dropTables(testPsqlConf)
	storage := newPsqlStateStorage(testPsqlConf)
	return storage
}

func newPsqlStateStorage(conf types.PostgresqlConfig) *adapters.TFStateStoragePostgres {
	return adapters.NewTFStateStoragePostgres(conf)
}

// drop 2 tables
func dropTables(conf types.PostgresqlConfig) {
	logger := log.WithFields(log.Fields{
		"package":  "tests",
		"function": "dropTables",
	})
	db, err := adapters.OpenPsql(conf)
	if err != nil {
		logger.WithError(err).Fatal("fail to open psql")
	}
	defer db.Close()
	_, err = db.Exec(`DROP TABLE tf_state;`)
	if err != nil {
		logger.WithError(err).Error("fail to drop table tf_state")
	}
	_, err = db.Exec(`DROP TABLE tf_history;`)
	if err != nil {
		logger.WithError(err).Error("fail to drop table tf_history")
	}
}
